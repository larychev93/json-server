module.exports = function () {
  const faker = require('faker')
  const _ = require('lodash')

  const MIN_YEAR_CREATED = 1900
  const MAX_YEAR_CREATED = 2010

  let books = []
  let categoryCounter = 0
  let step = 0

  function getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

  return {
    users: _.times(100, function (n) {
      return {
        id: n + 1,
        name: faker.name.firstName(),
        email: faker.internet.email(),
        password: faker.internet.password().slice(0, 5),
        surname: faker.name.lastName(),
        role: _.sample(['Reader', 'Librarian', 'Senior Librarian']),
        avatar: faker.image.avatar(),
        created_at: faker.date.past(),
        phone: faker.phone.phoneNumber(),
        blacklisted: false
      }
    }),
    books: _.times(100, function (n) {
      if (categoryCounter === 10) categoryCounter = 0

      ++categoryCounter

      let book = {
        id: n + 1,
        name: faker.commerce.productName(),
        author: getRandomArbitrary(1, 101),
        category:  categoryCounter,
        pages: getRandomArbitrary(1, 101),
        image: faker.random.image(),
        owner: null,
        description: faker.lorem.text(),
        created_at: getRandomArbitrary(MIN_YEAR_CREATED, MAX_YEAR_CREATED)
      }

      books.push(book)

      return book
    }),
    authors: _.times(100, function (n) {
      let diedAt = faker.date.past()
      let bornAt = faker.date.past()

      diedAt = diedAt >= bornAt ? diedAt : bornAt

      return {
        id: n + 1,
        name: faker.name.firstName() + ' ' + faker.name.lastName(),
        born_at: bornAt,
        died_at: diedAt,
        country: faker.address.country()
      }
    }),
    categories: _.times(20, (n) => {
      let id = n + 1
      let parentId = null

      ++step

      if (step > 5) {
        parentId = getRandomArbitrary(1, 5)
      }

      return {
        id: id,
        name: faker.commerce.department(),
        parent_id: parentId
      }
    })
  }
}